"""
    Q.1. In weather.dat you’ll find daily weather data for Morristown, NJ for
    June 2002. Download this text file, then write a program to output the day
    number (column one) with the smallest temperature spread (the maximum
    temperature is the second column, the minimum the third column).
"""

from data import Data
from my_util import Util


# Weather class -> Data
class WeatherData(Data):

    def __init__(self, file_loc):
        super().__init__(file_loc)


# main method
def execute():
    # file location
    weather_obj = WeatherData('./../res/weather.dat')

    # index of data wanted
    day = 0
    max_temprature = 1
    min_temprature = 2
    weather_obj.extract_data(day, max_temprature, min_temprature)
    # weather_obj.print_data_dict()

    # calculating the result
    result = Util.find_min_item(weather_obj.data_dict)

    print('The day number with the smallest temperature spread :', result)


if __name__ == "__main__":
    execute()
