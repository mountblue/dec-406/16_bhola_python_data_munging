"""
    Parent Data class
"""
import re


class Data(object):

    def __init__(self, file_loc):
        """
            Initializing the Data class with file location of data.
        """
        self.file_loc = file_loc
        self.data_dict = {}

    def extract_data(self, key_index, *other_indexes):
        """
            Extracting the data based on the indexes given and
            making a dictionary using value at key index.
        """
        if len(other_indexes) == 0:
            print('At least one index required!')
            return
        with open(self.file_loc, 'r') as data_file:
            data_file_lines = data_file.readlines()
            for data_line in data_file_lines:
                other_indexes_data_list = []

                # removing extra spaces from both side
                data_line = data_line.strip()

                regex = re.compile(r'[\s]+')
                data_line = regex.sub(',', data_line)
                regex = re.compile(r'[\*]+')
                data_line = regex.sub('', data_line)
                data_line_list = data_line.split(',')

                [valid, other_indexes_data_list] =\
                    Data.get_data_other_index(other_indexes, data_line_list)

                if valid:
                    continue

                self.data_dict[data_line_list[key_index]] =\
                    other_indexes_data_list

    @staticmethod
    def get_data_other_index(other_indexes, data_line_list):
        """
            This is a helping method which take the list and check data is
            available at that index in list or not if available it make a
            list and return it.
        """
        valid = False
        other_indexes_data_list = []

        for index in other_indexes:
            try:
                if data_line_list[index] != '':
                    other_indexes_data_list.append(data_line_list[index])
                else:
                    valid = True
            except IndexError:
                valid = True
                continue

        return [valid, other_indexes_data_list]

    def print_data_dict(self):
        """
            This function print the data_dict variable of class if data is
            not available in the dictionary it print a message.
        """
        if len(self.data_dict) == 0:
            print('No data is dictionary!')
            return
        for key, value in self.data_dict.items():
            print(key, ' : ', value)
