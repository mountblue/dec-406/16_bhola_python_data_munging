"""
    My util class
"""


class Util(object):

    @staticmethod
    def find_items_value_diff(data_dict):
        """
            Its a method which take a dictionary and find difference of the
            values and make a new dictionary with new value and return it.
        """
        items_value_diff = {}
        first_val = 0
        second_val = 1
        for key, value in data_dict.items():
            try:
                value_diff = int(value[first_val]) - int(value[second_val])
                if value_diff < 0:
                    value_diff *= -1
                items_value_diff[key] = value_diff
            except ValueError:
                continue
        return items_value_diff

    @staticmethod
    def find_min_item(data_dict):
        """
            Find the items of which value is minimum and return its value.
        """
        items_value_diff = Util.find_items_value_diff(data_dict)

        if len(items_value_diff) == 0:
            print('Something went wrong in find the difference!')

        items_value_diff_sorted =\
            sorted(items_value_diff.items(), key=lambda kv: kv[1])

        # print(items_value_diff_sorted)

        first_index = 0
        min_item = items_value_diff_sorted[first_index][first_index]

        return min_item
