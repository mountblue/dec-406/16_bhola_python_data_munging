"""
    Q.2. The file "football.dat" contains the results from the English
    Premier League for 2001/2. The columns labeled ‘F’ and ‘A’ contain
    the total number of goals scored for and against each team in that
    season (so Arsenal scored 79 goals against opponents, and had 36 goals
    scored against them). Write a program to print the name of the team with
    the smallest difference in ‘for’ and ‘against’ goals.
"""

from data import Data
from my_util import Util


# football data class
class FootballData(Data):

    def __init__(self, file_loc):
        super().__init__(file_loc)


# main method
def execute():
    # file location
    football_obj = FootballData('./../res/football.dat')

    # data index
    team = 1
    for_ = 6
    against = 8
    football_obj.extract_data(team, for_, against)
    # football_obj.print_data_dict()

    # finding result
    result = Util.find_min_item(football_obj.data_dict)

    print('The name of the team with the smallest difference in ‘for’',
          'and ‘against’ goals : ', result)


if __name__ == "__main__":
    execute()
